import * as child_process from "child_process";
import fs from "fs";
import path from "path";
import * as snarkjs from "snarkjs";


async function circom_exists() {
    const child = child_process.spawn('circom');
    return new Promise((resolve) => {
        child.on('exit', () => {
            resolve(true);
        });
        child.on('error', () => {
            resolve(false);
        });
    });
}

function exec_circom(path_circuit: string, circom_options: string, show_stdout?: boolean ): boolean {
    try {
        const buf = child_process.execSync(
            `circom ${path_circuit} ${circom_options}`,
            {encoding: 'utf8'},
        );
        if (show_stdout)
            console.log(buf);
        return true;
    } catch (error) {
        //console.error('Error:', (error as { status: number; message: string; stderr: Buffer }).status, (error as Error).message, (error as { stderr: Buffer }).stderr.toString());
        return false;
    }
}

function create_zk_folders() {
    let paths = ['../zk/circuits', '../zk/zkeys', '../zk/verifiers'];
    paths.map(p => {
        let folder = path.resolve(__dirname, p);
        fs.mkdirSync(folder, { recursive: true });
    });
}

function compile_circuits(): boolean {
    const extension = "circom";
    const folder = "../circuits";
    const circuits = fs.readdirSync(path.resolve(__dirname, folder))
        .filter(e => e.endsWith(extension)).map(e => path.resolve(__dirname, folder, e));
    circuits.forEach((circuit: string) => {
        if (exec_circom(circuit, '--r1cs --sym --wasm -o zk/circuits', true) == false)
            return false;
    });
    return true;
}

async function readStreamFile(filePath: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
        const chunks: Buffer[] =[];
        const stream = fs.createReadStream(filePath);

        stream.on('data', (chunk: Buffer) => {
            chunks.push(chunk);
        });

        stream.on('end', () => {
            resolve(Buffer.concat(chunks));
        });

        stream.on('error', (err) => {
            reject(err);
        });
    });
}

async function generate_zkeys() {
    const r1cs_extension = ".r1cs";
    const zkey_extension = ".zkey";
    const vkey_extention = ".vkey.json";
    const circuits_folder = "../zk/circuits";
    const zkeys_folder = "../zk/zkeys";
    const vkeys_folder = "../zk/verifiers";
    //const ptau = fs.readFileSync(path.resolve(__dirname, "../powersOfTau28_hez_final_21.ptau"));
    const ptau = await readStreamFile(path.resolve(__dirname, "../powersOfTau28_hez_final_16.ptau"));
    const r1cs_files = fs.readdirSync(path.resolve(__dirname, circuits_folder))
        .filter(e => e.endsWith(r1cs_extension)).map(e => path.resolve(__dirname, circuits_folder, e));
    for (const r1cs_file of r1cs_files) {
        const r1cs = fs.readFileSync(r1cs_file);
        const zkey_file1 = path.resolve(__dirname, zkeys_folder, path.basename(r1cs_file,r1cs_extension) + "_0000" + zkey_extension);
        const zkey_file_final = path.resolve(__dirname, zkeys_folder, path.basename(r1cs_file,r1cs_extension) + "_final" + zkey_extension);
        await snarkjs.zKey.newZKey(r1cs, ptau, zkey_file1);
        await snarkjs.zKey.contribute(zkey_file1, zkey_file_final, "My contribution", "my random data");
        let vkey = await snarkjs.zKey.exportVerificationKey(zkey_file_final);
        fs.writeFileSync(path.resolve(__dirname, vkeys_folder, path.basename(r1cs_file,r1cs_extension)) + vkey_extention, JSON.stringify(vkey, null, 4));
    }
}

function copy_zk_files() {
    fs.cpSync("zk/zkeys", "dist/zk/zkeys", {recursive: true});
    fs.cpSync("zk/circuits", "dist/zk/circuits", {recursive: true});
    fs.cpSync("zk/verifiers", "dist/zk/verifiers", {recursive: true});
}

async function run() {
    let out = await circom_exists();
    if (out == false) {
        console.error("Circom is not installed.");
        process.exit(-1);
    }
    create_zk_folders();
    if (compile_circuits() == false) {
        console.error("Some circuits could not be compiled.");
        process.exit(-1);
    }
    await generate_zkeys();
    copy_zk_files();
    console.log("Done");
}

run().then(() => process.exit());