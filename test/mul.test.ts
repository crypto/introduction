import assert from "assert";
import { MulProver } from "../src/mul.prover";
import { MulVerifier } from "../src/mul.verifier";

describe("Test Multiplier", function () {

    it("Should prove and verify with valid data", async function() {
        let prover = new MulProver();
        let proof = await prover.prove(BigInt(3),BigInt(7));
        
        const verifier = new MulVerifier();
        const v = await verifier.verify(proof);
        assert(v);
    });

    it("Should not verify a wrong proof", async function() {
        let prover = new MulProver();
        let proof = await prover.prove(BigInt(3),BigInt(7));

        proof.public_signals[0] = BigInt(22);

        const verifier = new MulVerifier();
        const v = await verifier.verify(proof);
        assert(!v);
    });
});