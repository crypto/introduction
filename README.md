# Introduction to Zero-Knowledge Proofs and Circom

This project is part of the course [EISC-204: Cryptography](https://gitlab-pages.isae-supaero.fr/crypto/website/projects/introduction.html) at ISAE-SUPAERO.
Please refer to it for more information.
