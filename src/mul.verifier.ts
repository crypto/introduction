import { SnarkProof, Groth16Verify } from "./proof";
import path from "path";

/**
 * Class representing a verifier for a multiplication circuit using zk-SNARKs.
 */
export class MulVerifier {
    private _verifierKey: string;

    /**
     * Constructs a new MulVerifier instance.
     * Initializes the verifier key path.
     */
    constructor() {
        this._verifierKey = path.join(__dirname, `../zk/verifiers/mul.vkey.json`);
    }

    /**
     * Creates a new MulVerifier instance from a file path.
     *
     * @param {string} vkPath - The path to the verifier key file.
     * @returns {Promise<MulVerifier>} A promise that resolves to the created MulVerifier instance.
     */
    static async fromFile(vkPath: string): Promise<MulVerifier> {
        const verifier = new MulVerifier();
        verifier._verifierKey = vkPath;
        return verifier;
    }

    /**
     * Verifies a zk-SNARK proof.
     *
     * @param {SnarkProof} proof - The proof to be verified.
     * @returns {Promise<boolean>} A promise that resolves to a boolean indicating whether the proof is valid.
     */
    async verify(proof: SnarkProof): Promise<boolean> {
        return Groth16Verify(this._verifierKey, proof);
    }
}
