import { SnarkProof } from "./proof";
import * as snarkjs from "snarkjs";
import fs from "fs";
import path from "path";

/**
 * Class representing a prover for a multiplication circuit using zk-SNARKs.
 */
export class MulProver {
    private readonly wasm: Buffer;
    private readonly zKey: Buffer;

    /**
     * Constructs a new MulProver instance.
     * Reads the necessary wasm and zKey files from the filesystem.
     */
    constructor() {
        const basePath = path.join(__dirname, `../zk`);
        this.wasm = fs.readFileSync(`${basePath}/circuits/mul_js/mul.wasm`);
        this.zKey = fs.readFileSync(`${basePath}/zkeys/mul_final.zkey`);
    }

    /**
     * Generates a zk-SNARK proof for the multiplication of two big integers.
     *
     * @param {bigint} x - The first input to the multiplication circuit.
     * @param {bigint} y - The second input to the multiplication circuit.
     * @returns {Promise<SnarkProof>} A promise that resolves to the generated proof and public signals.
     */
    async prove(x: bigint, y: bigint): Promise<SnarkProof> {
        const { proof, publicSignals } = await snarkjs.groth16.fullProve(
            { x, y },
            this.wasm,
            this.zKey
        );
        return {
            proof: {
                a: [proof.pi_a[0], proof.pi_a[1]] as [bigint, bigint],
                b: [proof.pi_b[0].reverse(), proof.pi_b[1].reverse()] as [
                    [bigint, bigint],
                    [bigint, bigint]
                ],
                c: [proof.pi_c[0], proof.pi_c[1]] as [bigint, bigint],
            },
            public_signals: publicSignals
        };
    }
}
