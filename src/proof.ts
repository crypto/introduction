import * as snarkjs from 'snarkjs';
import fs from 'fs';

/**
 * Interface for a Proof object.
 */
export interface Proof {
    a: [bigint, bigint];
    b: [[bigint, bigint], [bigint, bigint]];
    c: [bigint, bigint];
}

/**
 * Interface for a SnarkProof object.
 */
export interface SnarkProof {
    proof: Proof,
    public_signals: bigint[]
}

/**
 * Converts a signal to a Buffer.
 * @param signal - The signal to be converted.
 * @returns A Buffer representation of the signal.
 */
export function signalToBuffer(signal: bigint): Buffer {
    let bigIntValue = BigInt(signal).toString(16);
    let hexString = bigIntValue.length % 2 === 0 ? bigIntValue : '0' + bigIntValue;
    while (hexString.length < 64) {
        hexString = '0' + hexString;
    }
    return Buffer.from(hexString, 'hex');
}

/**
 * Converts an array of signals to a Buffer.
 * @param signals - The array of signals to be converted.
 * @returns A Buffer representation of the array of signals.
 */
export function signalsToBuffer(signals: bigint[]): Buffer {
    return Buffer.concat(signals.map(signalToBuffer));
}

/**
 * Converts a Buffer to a signal.
 * @param buffer - The Buffer to be converted.
 * @returns A bigint representation of the Buffer.
 */
export function signalFromBuffer(buffer: Buffer): bigint {
    return BigInt('0x' + buffer.toString('hex'));
}

/**
 * Serializes a Proof object to a Buffer.
 * @param proof - The Proof object to be serialized.
 * @returns A Buffer representation of the Proof object.
 */
export function serializeProof(proof: Proof): Buffer {
    const a = Buffer.concat(proof.a.slice(0, 2).map(signalToBuffer));
    const b = Buffer.concat(proof.b.slice(0, 2).map(row => Buffer.concat(row.map(signalToBuffer))));
    const c = Buffer.concat(proof.c.slice(0, 2).map(signalToBuffer));
    return Buffer.concat([a, b, c]);
}

/**
 * Serializes an array of public signals to a Buffer.
 * @param public_signals - The array of public signals to be serialized.
 * @returns A Buffer representation of the array of public signals.
 */
export function serializePublicSignals(public_signals: bigint[]): Buffer {
    return Buffer.concat(public_signals.map(signalToBuffer));
}

/**
 * Serializes a SnarkProof object to a Buffer.
 * @param proof - The SnarkProof object to be serialized.
 * @returns A Buffer representation of the SnarkProof object.
 */
export function serializeSnarkProof(proof: SnarkProof): Buffer {
    const proofBuffer = serializeProof(proof.proof);
    const signalsBuffer = serializePublicSignals(proof.public_signals);
    return Buffer.concat([proofBuffer, signalsBuffer]);
}

/**
 * Deserializes a Buffer to a Proof object.
 * @param buffer - The Buffer to be deserialized.
 * @returns A Proof object representation of the Buffer.
 */
export function deserializeProof(buffer: Buffer): Proof {
    const a = [buffer.subarray(0, 32), buffer.subarray(32, 64)].map(signalFromBuffer) as [bigint, bigint];
    const b = [
        [buffer.subarray(64, 96), buffer.subarray(96, 128)],
        [buffer.subarray(128, 160), buffer.subarray(160, 192)]
    ].map(row => row.map(signalFromBuffer)) as [[bigint, bigint], [bigint, bigint]];
    const c = [buffer.subarray(192, 224), buffer.subarray(224, 256)].map(signalFromBuffer) as [bigint, bigint];
    return { a, b, c };
}

/**
 * Deserializes a Buffer to an array of public signals.
 * @param buffer - The Buffer to be deserialized.
 * @returns An array of public signals representation of the Buffer.
 */
export function deserializePublicSignals(buffer: Buffer): bigint[] {
    const signals = [];
    for (let i = 0; i < buffer.length; i += 32) {
        signals.push(signalFromBuffer(buffer.subarray(i, i + 32)));
    }
    return signals;
}

/**
 * Deserializes a Buffer to a SnarkProof object.
 * @param buffer - The Buffer to be deserialized.
 * @returns A SnarkProof object representation of the Buffer.
 */
export function deserializeSnarkProof(buffer: Buffer): SnarkProof {
    const proof = deserializeProof(buffer.subarray(0, 256));
    const signals = deserializePublicSignals(buffer.subarray(256));
    return { proof, public_signals: signals };
}

/**
 * Verifies a SnarkProof object using Groth16.
 * @param vkey_file - The file path of the verification key.
 * @param proof - The Circom Proof to be verified.
 * @param public_signals - The public signals to be used in the verification.
 * @returns A promise that resolves to a boolean indicating whether the proof is valid.
 */
export async function Groth16Verify(vkey_file: string, { proof, public_signals }: SnarkProof): Promise<boolean> {
    const vkey_content = fs.readFileSync(vkey_file, 'utf-8');
    const vKey = JSON.parse(vkey_content);
    const snarkProof = {
        pi_a: [...proof.a, '1'],
        pi_b: [
            [proof.b[0][1], proof.b[0][0]],
            [proof.b[1][1], proof.b[1][0]],
            ['1', '0'],
        ],
        pi_c: [...proof.c, '1'],
    };
    return await snarkjs.groth16.verify(vKey, public_signals, snarkProof);
}
