import {MulProver} from "../src/mul.prover";
import {deserializeSnarkProof, serializeSnarkProof} from "../src/proof";
import fs from "fs";
import {MulVerifier} from "../src/mul.verifier";

async function main() {
    const prover = new MulProver()
    let proof = await prover.prove(BigInt("3"), BigInt("7"));
    console.log(proof);
}

main().then(() => process.exit());